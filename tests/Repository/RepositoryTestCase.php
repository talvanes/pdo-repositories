<?php namespace Talba\Tests\PDO\Repositories\Repository;

abstract class RepositoryTestCase extends PHPUnit_Extensions_Database_TestCase {

	# instantiate pdo just once for clean-up/fixture
	/** @var \PDO */
	static private $pdo = null;

	# instantiate connection just once for test
	/** @var \PHPUnit_Extensions_Database_DB_IDatabaseConnection */
	private $conn = null;

	# fixture that all repository tests will need
	private $fixtures = [
		'contacts',
	];

	/**
	 * Returns fake database connection to be used for unit testing repositories.
	 *
	 * @return \PHPUnit_Extensions_Database_DB_IDatabaseConnection 
	 */
	final public function getConnection() {
		if (!$this->conn) {
			if (!self::$pdo) self::$pdo = new \PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASS']);
			$this->conn = $this->createDefaultDbConnection(self::$pdo, $GLOBALS['DB_NAME']);
		}

		return $this->conn;
	}

	/**
	 * Returns data set that represents records from all the tables in database.
	 *
	 * @param array $fixtures [optional]
	 * @return \PHPUnit_Extensions_Database_DataSet_CompositeDataSet
	 */
	final public function getDataSet(array $fixtures = []) {
		if (empty($fixtures)) $fixtures = $this->fixtures;

		$compositeDataSet = new \PHPUnit_Extensions_Database_DataSet_CompositeDataSet([]);
		$fixturePath = dirname(__DIR__) . '/fixtures';

		foreach ($fixtures as $fixture) {
			$path = "{$fixturePath}/{$fixture}.xml";

			$dataSet = $this->createXMLDataSet($path);
			$compositeDataSet->addDataset($dataSet);
		}

		return $compositeDataSet;
	}

	/**
	 * Helper function to set up test data within a test method
	 *
	 * @param array $fixtures
	 * @return void
	 */
	final public function loadDataSet(array $fixtures) {
		# set the new dataset
		$this->getDatabaseTester()->setDataSet($fixtures);

		# call setUp which defines the table and adds the rows
		$this->getDatabaseTester()->onSetUp();
	}


	// [NOTICE] One must implement one's own implementation of setUp() and tearDown() methods //

}
