<?php namespace Talba\PDO\Repositories\Repositories;

use Talba\PDO\Repositories\Contracts\RepositoryInterface;
use Talba\PDO\Repositories\Exceptions\RepositoryException;
use Slim\PDO\Database as SlimPDO;

abstract class RepositoryAbstract implements RepositoryInterface {

	/**
	 * @var \Slim\PDO\Database $pdo
	 */
	private $pdo;

	/**
	 * Constructor
	 */
	public function __construct(SlimPDO $pdo){
		$this->pdo = $pdo;
	}


	/**
	 * Specifiy class model name
	 */
	abstract public function getEntity();



	/**
	 *
	 * @param array $columns [optional] 
	 * @return array
	 */
	public function all(array $columns = []) {
		# TODO: implement this one
	}

	/**
	 *
	 * @param integer $perPage [optional]
	 * @param array $columns [optional]
	 * @return array
	 */
	public function paginate($perPage = 10, array $columns = []) {
		# TODO: implement this one too
	}


	/**
	 *
	 * @param integer|string $id
	 * @param array $columns [optional]
	 * @return mixed
	 */
	public function find($id, array $columns = []) {
		# TODO: implement this one too
	}

	/**
	 *
	 * @param string $field
	 * @param mixed $value
	 * @param array $columns [optional]
	 * @return mixed
	 */
	public function findBy($field, $value, array $columns = []) {
		# TODO: implement this one too
	}


	/**
	 *
	 * @param array $data
	 * @return mixed
	 */
	public function create(array $data) {
		# TODO: implement this one too
	}

	/**
	 *
	 * @param array $data
	 * @param integer|string $id
	 * @return mixed
	 */
	public function update(array $data, $id) {
		# TODO: implement this one too
	}


	/**
	 *
	 * @param integer|string $id
	 * @return mixed
	 */
	public function delete($id) {
		# TODO: implement this one too
	}


}
