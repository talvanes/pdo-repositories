<?php namespace Talba\PDO\Repositories\Contracts;

interface RepositoryInterface {

	public function all(array $columns = []);

	public function paginate($perPage = 10, array $columns = []);

	public function find($id, array $columns = []);

	public function findBy($field, $value, array $columns = []);

	public function create(array $data);

	public function update(array $data, $id);

	public function delete($id);

}
